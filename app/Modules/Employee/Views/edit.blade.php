@extends('backend.layouts.modal')
@section('title') <h5><i class="fa fa-edit"></i> Company edit</h5> @endsection
@section('content')
    {!! Form::open(['route'=>array('employees.update',\App\Libraries\Encryption::encodeId($employee->id)), 'method'=>'patch','id'=>'dataForm']) !!}
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('first_name','First Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('first_name',$employee->first_name,['class'=>$errors->has('first_name')?'form-control is-invalid':'form-control required','placeholder'=>'First name']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('last_name','Last Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('last_name',$employee->last_name,['class'=>$errors->has('last_name')?'form-control is-invalid':'form-control required','placeholder'=>'Last name']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('company_id','Last Name : ',['class'=>'required-star']) !!}
                    {!! Form::select('company_id',$companies,$employee->company_id,['class'=>$errors->has('last_name')?'form-control is-invalid':'form-control required','placeholder'=>'Select a company']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('email','Email : ',['class'=>'required-star']) !!}
                    {!! Form::email('email',$employee->email,['class'=>$errors->has('email')?'form-control is-invalid':'form-control required','placeholder'=>'Email address']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('phone','Phone',['class'=>'required-star']) }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">+88</span>
                        </div>
                        {!! Form::text('phone',$employee->phone,['class'=>$errors->has('phone')?'form-control is-invalid':'form-control required','minlength'=>'11','maxlength'=>'11','placeholder'=>'Phone']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <a href="{{ route('companies.index') }}" class="btn btn-warning"><i class="fa fa-backward"></i> Back</a>
        <button type="submit" class="btn float-right btn-primary" id="productCategorySubmit"><i class="fa fa-save"></i> Update</button>
    </div>
    {!! Form::close() !!}
@endsection
