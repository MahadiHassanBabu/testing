<?php

namespace App\Modules\Employee\Controllers;

use App\Libraries\Encryption;
use App\Modules\Company\Models\Company;
use App\Modules\Employee\Models\Employee;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employees'] = Employee::leftJoin('companies','companies.id','employees.company_id')->get([
            'employees.*',
            DB::raw("CONCAT(employees.first_name,' ', employees.first_name) as employee_full_name"),
            'companies.name as company_name'
        ]);

        return view("Employee::index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['companies'] = Company::pluck('name','id');
        return view("Employee::create",$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'company_id' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()
                ]);
            }

            $checkExistUserEmail = User::where('email',$request->input('email'))->first();
            if($checkExistUserEmail)
                throw new \Exception("Account already exist by this email.<br> Please try with another email !", 123);

            DB::beginTransaction();
            $employee = new Employee();
            $employee->first_name = $request->get('first_name');
            $employee->last_name = $request->get('last_name');
            $employee->company_id = $request->get('company_id');
            $employee->email = $request->get('email');
            $employee->phone = $request->get('phone');
            $employee->password = $request->get('password');
            $employee->status = 1;
            $employee->save();

            $newAuthEmployee = new User();
            $newAuthEmployee->employee_id = $employee->id;
            $newAuthEmployee->name = $employee->first_name.' '.$employee->last_name;
            $newAuthEmployee->email = $employee->email;
            $newAuthEmployee->password = Hash::make($employee->password);
            $newAuthEmployee->status = 1;
            $newAuthEmployee->created_at = Carbon::now()->format('Y-m-d','H:i:s');
            $newAuthEmployee->created_by = auth()->user()->id;
            $newAuthEmployee->updated_at = Carbon::now()->format('Y-m-d','H:i:s');
            $newAuthEmployee->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'status' => 'Employee and new account created successfully.',
                'link' => route('employees.index')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'status' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $employeeId
     * @return \Illuminate\Http\Response
     */
    public function edit($employeeId)
    {
        $decodedEmployeeId = Encryption::decodeId($employeeId);
        $data['employee'] = Employee::find($decodedEmployeeId);
        $data['companies'] = Company::pluck('name','id');

        return view('Employee::edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $employeeId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $employeeId)
    {
        try {
            $validation = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()
                ]);
            }

            $checkExistUserEmail = User::where('email',$request->input('email'))->first();
            if($checkExistUserEmail)
                throw new \Exception("Account already exist by this email.<br> Please try with another email !", 123);

            DB::beginTransaction();
            $decodedEmployeeId = Encryption::decodeId($employeeId);
            $employee = Employee::find($decodedEmployeeId);
            $employee->first_name = $request->get('first_name');
            $employee->last_name = $request->get('last_name');
            $employee->company_id = $request->get('company_id');
            $employee->email = $request->get('email');
            $employee->phone = $request->get('phone');
            $employee->status = 1;
            $employee->save();

            User::where('employee_id',$decodedEmployeeId)->update([
                'employee_id' => $decodedEmployeeId,
                'name' => $employee->first_name.' '.$employee->last_name,
                'email' => $employee->email,
                'updated_by' => auth()->user()->id,
                'updated_at' => Carbon::now()->format('Y-m-d','H:i:s'),
            ]);
            DB::commit();

            return response()->json([
                'success' => true,
                'status' => 'Employee and employee account updated successfully.',
                'link' => route('employees.index')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'status' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $employeeId
     * @return \Illuminate\Http\Response
     */
    public function delete($employeeId)
    {
        $decodedEmployeeId = Encryption::decodeId($employeeId);
        Employee::where('id',$decodedEmployeeId)->delete();
        User::where('employee_id',$decodedEmployeeId)->delete();
        session()->flash('success','Employee and account deleted successfully.');
    }
}
