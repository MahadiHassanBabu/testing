<?php

Route::group(['module' => 'Employee', 'middleware' => ['web'], 'namespace' => 'App\Modules\Employee\Controllers'], function() {

    Route::get('employees/{id}/delete','EmployeeController@delete');
    Route::resource('employees', 'EmployeeController');

});
