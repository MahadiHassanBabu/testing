<?php

namespace App\Modules\Employee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Employee extends Model {

    protected $table = 'employees';
    protected $fillable = [
        'id',
        'company_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'status',
        'is_archive',
        'created_by',
        'updated_by',
        'deleted_by',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($data) {
            $data->created_by = Auth::user()->id;
            $data->updated_by = Auth::user()->id;
        });

        static::updating(function($data) {
            $data->updated_by = Auth::user()->id;
        });
    }

}
