@extends('backend.layouts.app')
@section('content')
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-12">Welcome &nbsp;<strong class="text-success">{{ auth()->user()->name }}</strong>&nbsp;to Dashboard.</div>
    </div>
    <!-- /.row -->
@endsection
