@extends('backend.layouts.modal')
@section('title') <h5><i class="fa fa-plus-square"></i> Company create</h5> @endsection
@section('content')
    {!! Form::open(['route'=>'companies.store', 'method'=>'post','id'=>'dataForm']) !!}
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('name','Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('name','',['class'=>$errors->has('name')?'form-control is-invalid':'form-control required','placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('email','Email : ',['class'=>'required-star']) !!}
                    {!! Form::email('email','',['class'=>$errors->has('email')?'form-control is-invalid':'form-control required','placeholder'=>'Email address']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('website','Website : ',['class'=>'required-star']) !!}
                    {!! Form::text('website','',['class'=>$errors->has('website')?'form-control is-invalid':'form-control required','placeholder'=>'Website link']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('status','Status : ',['class'=>'required-star']) !!}
                    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],'',['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('logo', 'Logo :',['class'=>'required-star']) }}
                <br/>
                <span id="photo_err" class="text-danger" style="font-size: 15px;"></span>
                <div>
                    <img class="img img-responsive img-thumbnail"
                         src="{{ url('assets/backend/img/photo.png') }}" id="photoViewer"
                         height="150" width="130">
                </div>
                <label class="btn btn-block btn-secondary btn-sm border-0">
                    <input onchange="changePhoto(this)" type="file" name="logo" style="display: none" required>
                    <i class="fa fa-image"></i> Browse
                </label>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <a href="{{ route('companies.index') }}" class="btn btn-warning"><i class="fa fa-backward"></i> Back</a>
        <button type="submit" class="btn float-right btn-primary" id="productCategorySubmit"><i class="fa fa-save"></i> Save</button>
    </div>
    {!! Form::close() !!}
@endsection
@section('footer-script')
    <script type="text/javascript">

        /********************
         VALIDATION START HERE
         ********************/
        $(document).ready(function () {
            $('#dataForm').validate({
                errorPlacement: function () {
                    return false;
                }
            });
        });
    </script>

@endsection
