@extends('backend.layouts.app')
@section('header-css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/backend/dist/css/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/backend/dist/css/buttons.dataTables.min.css') }}"/>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-5">
                    <h5><i class="fa fa-list-alt"></i> Companies List</h5>
                </div><!--col-->

                <div class="col-sm-7 pull-right">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('companies.create') }}"
                           class="btn btn-sm btn-success ml-1 AppModal"
                           data-toggle="modal"
                           data-target="#AppModal"
                           title="Create new"
                           data-original-title="Create New">
                            <i class="fa fa-plus-circle"></i> Create
                        </a>
                    </div>
                </div><!--col-->
            </div>
        </div>
        <div class="card-body">
            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table table-striped table-borderless">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Logo</th>
                        <th>Website</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if(isset($companies))
                            @foreach($companies as $company)
                                <tr>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->email }}</td>
                                    <td><img src="{{ url('/uploads/company-logo/'.$company->logo) }}" alt="" height="50" width="60"></td>
                                    <td>{{ $company->website }}</td>
                                    <td>
                                        <a href="{{ url('/companies/'.\App\Libraries\Encryption::encodeId($company->id)).'/edit/' }}" class="btn btn-sm btn-primary AppModal" title="Edit" data-toggle='modal' data-target='#AppModal'><i class="fa fa-edit"></i> Edit</a>
                                        <a href="{{ url('/companies/'.\App\Libraries\Encryption::encodeId($company->id)).'/delete/' }}"
                                           redirect-url="{{ url('/companies') }}"
                                           class="btn btn-sm btn-danger action-delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
    @include('backend.includes.modal-dialogue-md')

@endsection

@section('footer-script')
    <script src="{{ asset('assets/backend/dist/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/dist/js/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/dist/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $('table').DataTable();
        $(document.body).on('click','.action-delete',function(ev){
            ev.preventDefault();
            let URL = $(this).attr('href');
            let redirectURL = "{{ route('companies.index') }}";
            warnBeforeAction(URL, redirectURL);
        });

        function changePhoto(input) {
            if (input.files && input.files[0]) {
                $("#photo_err").html('');
                let mime_type = input.files[0].type;
                if (!(mime_type == 'image/jpeg' || mime_type == 'image/jpg' || mime_type == 'image/png')) {
                    $("#photo_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoViewer').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
