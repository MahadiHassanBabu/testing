<?php

namespace App\Modules\Company\Controllers;

use App\Libraries\Encryption;
use App\Modules\Company\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['companies'] = Company::all();
        return view("Company::index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Company::create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'website' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()
                ]);
            }

            $company = new Company();
            $company->name = $request->get('name');
            $company->email = $request->get('email');
            $company->name = $request->get('name');
            $company->website = $request->get('website');
            $company->status = 1;

            $path = 'uploads/company-logo/';
            if($request->hasFile('logo')){
                $_companyLogo = $request->file('logo');
                $mimeType = $_companyLogo->getClientMimeType();
                if(!in_array($mimeType,['image/jpg', 'image/jpeg', 'image/png']))
                    return redirect()->back();
                if(!file_exists($path))
                    mkdir($path, 0777, true);

                $companyLogo = trim(sprintf('%s', uniqid('CompanyLogo_', true))) . '.' . $_companyLogo->getClientOriginalExtension();
                Image::make($_companyLogo->getRealPath())->resize(300,300)->save($path . '/' . $companyLogo);
                $company->logo = $companyLogo;
            }

            $company->save();

            return response()->json([
                'success' => true,
                'status' => 'Company created successfully.',
                'link' => route('companies.index')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'status' => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $companyId
     * @return \Illuminate\Http\Response
     */
    public function edit($companyId)
    {
        $decodedId = Encryption::decodeId($companyId);
        $data['company'] = Company::find($decodedId);
        return view("Company::edit",$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $companyId)
    {
        try {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'website' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()
                ]);
            }

            $decodedId = Encryption::decodeId($companyId);
            $company = Company::find($decodedId);
            $company->name = $request->get('name');
            $company->email = $request->get('email');
            $company->name = $request->get('name');
            $company->website = $request->get('website');
            $company->logo = $request->get('logo');
            $company->status = 1;

            if($request->hasFile('logo')){
                $path = 'uploads/company-logo/';
                $_companyLogo = $request->file('logo');
                $mimeType = $_companyLogo->getClientMimeType();
                if(!in_array($mimeType,['image/jpg', 'image/jpeg', 'image/png']))
                    return redirect()->back();

                $previousLogo = $path.'/'.$company->logo; // get previous image from folder
                if (File::exists($previousLogo)) // unlink or remove previous image from folder
                    @unlink($previousLogo);

                if(!file_exists($path))
                    mkdir($path, 0777, true);

                $companyLogo = trim(sprintf('%s', uniqid('CompanyLogo_', true))) . '.' . $_companyLogo->getClientOriginalExtension();
                Image::make($_companyLogo->getRealPath())->resize(300,300)->save($path . '/' . $companyLogo);
                $company->logo = $companyLogo;
            }

            $company->save();

            return response()->json([
                'success' => true,
                'status' => 'Company updated successfully.',
                'link' => route('companies.index')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'status' => $e->getMessage()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $companyId
     * @return \Illuminate\Http\Response
     */
    public function delete($companyId)
    {
        $decodedId = Encryption::decodeId($companyId);
        Company::where('id',$decodedId)->delete();
        session()->flash('success','Company deleted successfully');
    }
}
